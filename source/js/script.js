// burger-menu

var mainNav = document.querySelector('.main-nav');
var burger = document.querySelector('.main-nav__burger');

mainNav.classList.remove('main-nav--nojs');

burger.addEventListener('click', function() {
  if (mainNav.classList.contains('main-nav--closed')) {
    mainNav.classList.remove('main-nav--closed');
    mainNav.classList.add('main-nav--opened');
  }  else  {
    mainNav.classList.add('main-nav--closed');
    mainNav.classList.remove('main-nav--opened')
  }
});



// owl

$(document).ready(function(){
  $(".owl-carousel").owlCarousel({
    nav: false,
    navText : ["",""],
    center: true,
    loop: true,
    items: 1,
    autoHeight: true,

    responsive: {
      1020: {
        dots: false,
        nav: true,
        autoHeight: false,
      }
    }
    
  });
});